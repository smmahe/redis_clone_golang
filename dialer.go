package main

import (
	"fmt"
	"net"
	"os"
	"bufio"
	"time"
	"strings"
	"io"
)

func readResponse(conn net.Conn){
	buf := make([]byte,4096)
	n,err := bufio.NewReader(conn).Read(buf)
	fmt.Println("Read bytes count:%d",n)
	if err != nil{
		if err != io.EOF{
			fmt.Println("Error reading")
		}
	}
	data := string(buf)
	data_parts:=strings.Split(string(data),"\r\n")
	for i:=0;i<len(data_parts);i++{
		if strings.Contains(data_parts[i],"$") || strings.Contains(data_parts[i],"ECHO") || strings.Contains(data_parts[i],"*"){
			continue
		}
		fmt.Println(data_parts[i])
	}		
}

func main() {
	// You can use print statements as follows for debugging, they'll be visible when running tests.
	conn, err := net.Dial("tcp", "127.0.0.1:3333")
	conn.SetReadDeadline(time.Now().Add(2 * time.Minute))
	var delimiter,data string
	var sz int32
	delimiter = "\\r\\n"
	if err != nil {
		fmt.Println("Failed to connect to port 3333")
		os.Exit(1)
	}
	for{
		sz+=1
		fmt.Println("------Loop dialer------- %d",sz)
		reader:= bufio.NewReader(os.Stdin)
		fmt.Println("Enter data for sending:")
		user_inp, _ := reader.ReadString('\n')
		user_inp = strings.TrimSuffix(user_inp, "\n")
		if user_inp == "END"{
			break
		}else{
			data = data + "$" + fmt.Sprint(len(user_inp)) + delimiter + user_inp + delimiter
		}
	}

	prefix:= "*"+fmt.Sprint(sz)+delimiter //+ "$4\r\necho\r\n"
	data = prefix + data
	fmt.Fprintf(conn, data)
    readResponse(conn)
    defer conn.Close()
}