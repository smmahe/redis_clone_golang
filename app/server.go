package main

import (
	"fmt"
	"net"
	"os"
	"bufio"
	"strings"
	"io"
	"time"
)

type Key struct{
	data string
	expiry time.Time
	toexpire bool
}

var Db map[Key]string

func init(){
	Db = make(map[Key]string)
}


func set (key string, val string){
	new_entry:=Key{data:key,expiry:time.Now(),toexpire:false}
	Db[new_entry] = val
	fmt.Print("Inserting key:",key)
	fmt.Println(" Value:",val)

}

func parse_resp_array(data string) string {
	data_parts := strings.Split(string(data),"\\r\\n")
	var ok string
	var res string
	for i:=0;i<len(data_parts);i++{
		if data_parts[i] == "set"{
			if i+2 >len(data_parts)-1 || i+4 > len(data_parts){
				fmt.Print("Error")
				return "-error no value to set\r\n"
			}
			set(data_parts[i+2],data_parts[i+4])
			ok = "+OK\r\n"
			return ok
		}else if data_parts[i] == "echo"{
			res = strings.Join(data_parts,"")
			return res
		} 
	}
	res = strings.Join(data_parts,"")
	return res
}

func handleconn(conn net.Conn){
	for{
		buf := make([]byte,4096)
		n,err := bufio.NewReader(conn).Read(buf)
		fmt.Println("Read bytes count:%d",n)
		if err != nil{
			if err != io.EOF{
				fmt.Println("Error reading")
			}
			break
		}
		data := string(buf)
		fmt.Println("Encoded data:",data)
		response := parse_resp_array(data)
		
		fmt.Println("Sending response:",response)
		//data = strings.TrimSuffix(data, "\n")
		// var response []byte
		// if data == "PING"{
		// 	response = []byte("+PONG\r\n")
		// }
		// response = data
		conn.Write(buf)
	}
}

func main() {
	// You can use print statements as follows for debugging, they'll be visible when running tests.
	fmt.Println("Logs from your program will appear here!")

	l, err := net.Listen("tcp", "127.0.0.1:3333")
	fmt.Println(err)
	if err != nil {
		fmt.Println("Failed to bind to port 3333")
		os.Exit(1)
	}
	defer l.Close()
	
	fmt.Println("----Loop----")
	for{
		conn, err := l.Accept()
		fmt.Println(conn)
		if err != nil {
			fmt.Println("Error accepting connection: ", err.Error())
			os.Exit(1)
		}
		defer conn.Close()
		go handleconn(conn)
	}
	
}
